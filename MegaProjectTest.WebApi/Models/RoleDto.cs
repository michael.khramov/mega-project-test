﻿using MegaProjectTest.Core.Domain;

namespace MegaProjectTest.WebApi.Models
{
    /// <summary>
    /// Role Dto
    /// </summary>
    public class RoleDto: BaseEntity
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
